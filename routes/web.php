<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('master');
});

Route::get('/dashboard', function () {
    return view('master_parts.dashboard');
});

Route::get('/table', function () {
    return view('master_parts.table');
});

Route::get('/data-tables', function () {
    return view('master_parts.data_tables');
});


